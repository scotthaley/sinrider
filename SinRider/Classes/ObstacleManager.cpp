#include "ObstacleManager.h"

USING_NS_CC;

bool ObstacleManager::init()
{
	if (!Layer::init())
		return false;

	this->_winSize = Director::getInstance()->getVisibleSize();

	return true;
}

void ObstacleManager::setSinWave(SinWave* wave)
{
	this->_sinWave = wave;
}

void ObstacleManager::generateChunk()
{
	float height = 1;
	for (int x = 500; x <= 10000; x += 70)
	{
		this->generateWall(x, 60 + fabs(height) * 80, 73, 1, height, WallType::CENTER);
		height -= 0.02;
	}
}

void ObstacleManager::generateWall(float x, float offset, float width, float sinFreq, float sinHeight, WallType type)
{
	auto newWall = DrawNode::create();
	newWall->setPosition(this->getPositionX() + x, 0);
	float sinY = this->_sinWave->getSinY(x, sinFreq, sinHeight);

	auto color = Color4F(0.06, 0.29, 0.62, 1.0);
	auto border = Color4F(0, 0.68, 0.94, 1.0);

	switch (type)
	{
	case BOTTOM:
		this->DrawRect(newWall, Vec2(-width / 2, 0),
			Vec2(width / 2, sinY - offset), color, border);
		break;
	case CENTER:
		this->DrawRect(newWall, Vec2(-width / 2, 0),
			Vec2(width / 2, sinY - offset), color, border);
		this->DrawRect(newWall, Vec2(-width / 2, this->_winSize.height),
			Vec2(width / 2, sinY + offset), color, border);
		break;
	}

	this->addChild(newWall);
}

void ObstacleManager::DrawRect(DrawNode* node, Vec2 origin, Vec2 destination, Color4F color, Color4F border)
{
	auto borderSize = 3;
	node->drawSolidRect(origin, destination, border);
	auto xOrder = origin.x > destination.x ? -1 : 1;
	auto yOrder = origin.y > destination.y ? -1 : 1;
	node->drawSolidRect(Vec2(origin.x + borderSize * xOrder, origin.y + borderSize * yOrder),
		Vec2(destination.x - borderSize * xOrder, destination.y - borderSize * yOrder), color);
}

void ObstacleManager::update(float dt)
{

}