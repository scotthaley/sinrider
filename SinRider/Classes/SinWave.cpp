#include "SinWave.h"

USING_NS_CC;

bool SinWave::init()
{
	if (!Node::init())
		return false;

	this->_sinHeight = 0;
	this->_sinPosition = 0;
	this->_sinFrequency = 1;
	this->_desiredHeight = 0;
	this->_desiredFrequency = 1;

	this->_DrawNode = DrawNode::create();
	this->addChild(this->_DrawNode);

	return true;
}

void SinWave::updateWave()
{
	this->_DrawNode->clear();

	if (fabs(this->_sinHeight - this->_desiredHeight) > 0.1)
		this->_sinHeight -= (this->_sinHeight - this->_desiredHeight) / 5;
	else
		this->_sinHeight = this->_desiredHeight;

	if (fabs(this->_sinFrequency - this->_desiredFrequency) > 0.1)
		this->_sinFrequency -= (this->_sinFrequency - this->_desiredFrequency) / 5;
	else
		this->_sinFrequency = this->_desiredFrequency;

	int tp = 50 * this->_sinFrequency;
	float inc = w_w / tp;

	Vec2 lastPoint;

	for (int i = 0; i <= tp; i++)
	{
		Vec2 nextPoint = Vec2(inc * i, this->getSinY(inc * i));
		if (i != 0)
			this->_DrawNode->drawSegment(lastPoint, nextPoint, 1.0, Color4F(0.9, 0.9, 0.9, 1));
		lastPoint = nextPoint;
	}
}


float SinWave::getSinY(float x, float freq, float height)
{
	return 50 + w_m + this->getSinBase(x, freq) * height * w_h / 2;
}

float SinWave::getSinY(float x)
{
	return this->getSinY(x, this->_sinFrequency, this->_sinHeight);
}

float SinWave::getSinSlope(float x, float exageration = 1)
{
	return cos(this->getFunctionBase(x, this->_sinFrequency)) * -(this->_sinHeight * this->_sinFrequency * exageration);
}

float SinWave::getSinAngle(float x, float exageration = 1)
{
	return atan(this->getSinSlope(x, exageration)) * 180 / M_PI;
}

float SinWave::getSinBase(float x, float freq)
{
	return sin(this->getFunctionBase(x, freq));
}

float SinWave::getFunctionBase(float x, float freq)
{
	float x_offset = x / w_w * (2 * M_PI);
	float pos_offset = this->_sinPosition / w_w * (2 * M_PI);
	return pos_offset + x_offset * freq;
}

void SinWave::setDesired(float height, float freq)
{
	this->_desiredHeight = height;
	this->_desiredFrequency = freq;
}

void SinWave::setSinPosition(float pos)
{
	_sinPosition = pos;
}

void SinWave::setSinHeight(float height)
{
	this->_sinHeight = height;
}

void SinWave::setSinFrequency(float freq)
{
	this->_sinFrequency = freq;
}

void SinWave::setWinSize(Size size)
{
	this->_winSize = Size(size);
	this->w_h = this->_winSize.height - 100;
	this->w_w = this->_winSize.width - 100;
	this->w_m = w_h / 2;
}