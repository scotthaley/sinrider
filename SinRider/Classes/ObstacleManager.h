#pragma once

#include "cocos2d.h"
#include "SinWave.h"

enum WallType
{
	BOTTOM = 0,
	TOP,
	CENTER,
	SPLIT,
	SPLIT_CENTER
};

class ObstacleManager : public cocos2d::Layer
{
private:
	SinWave* _sinWave;
	cocos2d::Size _winSize;
public:
	virtual bool init();

	void update(float) override;
	void setSinWave(SinWave*);

	void generateWall(float, float, float, float, float, 
		WallType);
	void generateChunk();

	void DrawRect(cocos2d::DrawNode* node, cocos2d::Vec2 origin, cocos2d::Vec2 destination, cocos2d::Color4F color, cocos2d::Color4F border);

	CREATE_FUNC(ObstacleManager);
};