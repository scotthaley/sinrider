#pragma once

#include "cocos2d.h"
#include "SinWave.h"
#include "ObstacleManager.h"

class GameScene : public cocos2d::LayerColor
{
private:
	SinWave* _SinWave;

	ObstacleManager* _ObstacleManager;

	cocos2d::DrawNode* _PlayerSprite;

	cocos2d::DrawNode* _Slider;
	float _sliderHeight;
	cocos2d::Touch* _sliderTouch;
	cocos2d::Touch* _outsideTouch;

	float _PlayerOffset;

	cocos2d::Size _winSize;
public:
	static cocos2d::Scene* createScene();
	virtual bool init();

	void update(float) override;

	virtual void onTouchesBegan(const std::vector<cocos2d::Touch*>&, cocos2d::Event*);
	virtual void onTouchesMoved(const std::vector<cocos2d::Touch*>&, cocos2d::Event*);
	virtual void onTouchesEnded(const std::vector<cocos2d::Touch*>&, cocos2d::Event*);

	void drawSlider();

	CREATE_FUNC(GameScene);
};

