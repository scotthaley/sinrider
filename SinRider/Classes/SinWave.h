#pragma once

#include "cocos2d.h"

class SinWave : public cocos2d::Node
{
private:
	cocos2d::DrawNode* _DrawNode;

	float _sinHeight;
	float _sinPosition;
	float _sinFrequency;
	float _desiredHeight;
	float _desiredFrequency;

	float w_h;
	float w_w;
	float w_m;

	cocos2d::Size _winSize;

public:
	virtual bool init();
	CREATE_FUNC(SinWave);

	void setWinSize(cocos2d::Size);
	void setSinPosition(float);
	void setSinHeight(float);
	void setSinFrequency(float);
	void setDesired(float, float);
	void updateWave();

	float getSinY(float x, float freq, float height);
	float getSinY(float x);
	float getSinSlope(float, float);
	float getSinAngle(float, float);
	float getSinBase(float, float);
	float getFunctionBase(float x, float freq);
};

