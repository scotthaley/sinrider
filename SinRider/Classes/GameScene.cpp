#include "GameScene.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
	auto scene = Scene::create();
	auto layer = GameScene::create();
	scene->addChild(layer);
	return scene;
}

bool GameScene::init()
{
	if (!LayerColor::initWithColor(Color4B(46,49,146,255)))
		return false;

	this->_winSize = Director::getInstance()->getVisibleSize();

	this->_PlayerOffset = 0;

	this->_SinWave = SinWave::create();
	this->_SinWave->setWinSize(this->_winSize);
	this->addChild(this->_SinWave);

	this->_ObstacleManager = ObstacleManager::create();
	this->_ObstacleManager->setSinWave(this->_SinWave);
	this->_ObstacleManager->generateChunk();
	this->addChild(this->_ObstacleManager);

	//this->_PlayerSprite = Sprite::create("player.png");
	this->_PlayerSprite = DrawNode::create();
	this->_PlayerSprite->drawSolidCircle(Vec2(0, 0), 20, 0, 20, Color4F(1, 1, 1, 1));
	//this->_PlayerSprite->setAnchorPoint(Vec2(0.9f, 0.5f));
	this->addChild(this->_PlayerSprite);

	this->_sliderHeight = this->_winSize.height / 2;
	this->_Slider = DrawNode::create();
	this->addChild(this->_Slider);

	this->_sliderTouch = NULL;
	this->_outsideTouch = NULL;

	auto touchListener = EventListenerTouchAllAtOnce::create();
	touchListener->onTouchesBegan = CC_CALLBACK_2(GameScene::onTouchesBegan, this);
	touchListener->onTouchesMoved = CC_CALLBACK_2(GameScene::onTouchesMoved, this);
	touchListener->onTouchesEnded = CC_CALLBACK_2(GameScene::onTouchesEnded, this);

	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	this->scheduleUpdate();

	return true;
}

void GameScene::update(float dt)
{
	this->_PlayerOffset += 100.0 * dt;

	this->_SinWave->setSinPosition(this->_PlayerOffset);
	this->_SinWave->updateWave();

	this->_ObstacleManager->setPositionX(-this->_PlayerOffset);

	this->_PlayerSprite->setPosition(100, this->_SinWave->getSinY(100));
	this->_PlayerSprite->setRotation(this->_SinWave->getSinAngle(100, 2));

	this->drawSlider();
	this->_SinWave->setDesired((this->_sliderHeight - 50) * 2 / (this->_winSize.height - 100) - 1, 
		(this->_outsideTouch != NULL) + 1);
}

void GameScene::drawSlider()
{
	if (this->_sliderHeight < 50)
		this->_sliderHeight = 50;
	if (this->_sliderHeight > this->_winSize.height - 50)
		this->_sliderHeight = this->_winSize.height - 50;
	this->_Slider->clear();
	this->_Slider->drawSolidRect(Vec2(this->_winSize.width - 100, 0), Vec2(this->_winSize.width, this->_winSize.height), Color4F(0, 0, 0.188, 1));
	this->_Slider->drawSolidRect(Vec2(this->_winSize.width - 100, this->_winSize.height / 2), Vec2(this->_winSize.width, this->_sliderHeight), Color4F(0.9, 0.9, 0.9, 1));
	this->_Slider->drawSolidRect(Vec2(this->_winSize.width - 100, this->_winSize.height / 2), Vec2(this->_winSize.width, this->_winSize.height / 2 - (this->_sliderHeight - this->_winSize.height / 2)), Color4F(0.5, 0.5, 0.5, 1));
}

void GameScene::onTouchesBegan(const std::vector<Touch*>& Touches, Event* Event)
{
	for (int i = 0; i < Touches.size(); i++)
	{
		auto touch = Touches[i];
		if (this->_sliderTouch == NULL && touch->getLocation().x > this->_winSize.width - 100)
		{
			this->_sliderTouch = touch;
			this->_sliderHeight = touch->getLocation().y;
		}
		else if(this->_outsideTouch == NULL) {
			this->_outsideTouch = touch;
		}
	}
}

void GameScene::onTouchesMoved(const std::vector<Touch*>& Touches, Event* Event)
{
	for (int i = 0; i < Touches.size(); i++)
	{
		if (this->_sliderTouch == Touches[i])
			this->_sliderHeight = this->_sliderTouch->getLocation().y;
	}
}

void GameScene::onTouchesEnded(const std::vector<Touch*>& Touches, Event* Event)
{
	for (int i = 0; i < Touches.size(); i++)
	{
		if (this->_sliderTouch == Touches[i])
			this->_sliderTouch = NULL;
		if (this->_outsideTouch == Touches[i])
			this->_outsideTouch = NULL;
	}
}
